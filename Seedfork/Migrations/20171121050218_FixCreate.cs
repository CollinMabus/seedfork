﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Seedfork.Migrations
{
    public partial class FixCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Members_SourceId",
                table: "Transactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Members_DestinationId",
                table: "Transactions");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Members_SourceId",
                table: "Transactions",
                column: "SourceId",
                principalTable: "Members",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Members_DestinationId",
                table: "Transactions",
                column: "DestinationId",
                principalTable: "Members",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Members_SourceId",
                table: "Transactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Members_DestinationId",
                table: "Transactions");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Members_SourceId",
                table: "Transactions",
                column: "SourceId",
                principalTable: "Members",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Members_DestinationId",
                table: "Transactions",
                column: "DestinationId",
                principalTable: "Members",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
