﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Seedfork.Models
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {

        }

        public DbSet<Order.Transaction> Transactions { get; set; }
        public DbSet<Order.Item> Items { get; set; }
        public DbSet<Order.FarmerDonation> IncomingDonations { get; set; }
        public DbSet<Order.PantryDonation> OutgoingDonations { get; set; }
        // public DbSet<Order.OrderInfo> 
        public DbSet<Users.ContactInfo> MembersContactInfo { get; set; }
        public DbSet<Users.Member> Members { get; set; }
        public DbSet<Users.Farmer> Farmers { get; set; }
        public DbSet<Users.Pantry> Pantries { get; set; }
        public DbSet<Users.Address> Addresses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
        }
    }


}
