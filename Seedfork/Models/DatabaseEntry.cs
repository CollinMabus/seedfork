﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Seedfork.Models
{
    public class DatabaseEntry
    {
        [Key]
        public int ID { get; set; }
    }
}
