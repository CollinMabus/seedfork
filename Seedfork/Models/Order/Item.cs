﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Seedfork.Models.Order
{
    public class Item : DatabaseEntry
    {
        public string ItemName { get; set; }
        public float PricePerUnit { get; set; }
    }
}
