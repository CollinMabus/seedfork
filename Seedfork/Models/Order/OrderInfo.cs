﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Seedfork.Models.Order
{
    public class OrderInfo : DatabaseEntry
    {
        public int OrderItemId { get; set; }
        public Item OrderItem { get; set; }
        public int ItemCount { get; set; }
        public int ReferencedOrderId { get; set; }
        public FarmerDonation ReferencedOrder { get; set; }
    }
}
