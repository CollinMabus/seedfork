﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Seedfork.Models.Order
{
    public class Transaction : DatabaseEntry
    {
        public float TotalTransactionPrice => DonationItems.Sum(di => di.OrderItem.PricePerUnit);
        public DateTime Date { get; set; }
        public virtual ICollection<OrderInfo> DonationItems { get; set; }
    }
}