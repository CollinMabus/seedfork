﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Seedfork.Models.Order
{
    public class PantryDonation : Transaction
    {
        public int ? DestinationId { get; set; }
        public Users.Pantry Destination { get; set; }
    }
}