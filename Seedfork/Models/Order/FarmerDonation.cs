﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Seedfork.Models.Order
{
    public class FarmerDonation : Transaction
    {
        public int ? SourceId { get; set; }
        public Users.Farmer Source { get; set; }
    }
}