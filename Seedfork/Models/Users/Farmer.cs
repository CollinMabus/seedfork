﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Seedfork.Models.Users
{
    public class Farmer : Member
    {
        public virtual ICollection<Order.FarmerDonation> AllOrders { get; set; }
    }
}
