﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Seedfork.Models.Users
{
    public class Member : DatabaseEntry
    {
        public string UserName { get; set; }
        public int MemberContactId { get; set; }
        public ContactInfo MemberContact { get; set; }
    }
}
