﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Seedfork.Models.Users
{
    public class Address : DatabaseEntry
    {
        public string StreetAddress;
        public string City;
        public string ZIP;
        public string State;
    }
}
