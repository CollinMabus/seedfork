﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Seedfork.Models.Users
{
    public class Pantry : Member
    {
        public virtual ICollection<Order.PantryDonation> AllDonations { get; set; }
    }
}
